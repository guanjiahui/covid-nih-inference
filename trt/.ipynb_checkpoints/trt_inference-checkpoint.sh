#!/bin/bash

'''
baseDir="/home/ubuntu/Canon/NIH_covid/trt"
MODEL_NAME="classification_covid-19_v1"
#MODEL_NAME="class_trt"
#MODEL_NAME="segmentation_ct_lung_v1"
MODEL_NAME="segmentation_ct_lung_v1"
dockerImage=nvcr.io/nvidia/tritonserver:20.08-py3

docker run --gpus=1 --rm --shm-size=1g --ulimit memlock=-1 --ulimit stack=67108864\
       -p8000:8000 \
       -v $baseDir/models/${MODEL_NAME}:/models \
       ${dockerImage} tritonserver --model-repository=/models\
       
'''
baseDir="/home/ubuntu/Canon/NIH_covid/trt"
dockerImage=nvcr.io/nvidia/tensorrt:20.10-py3

docker run --gpus all -it --rm \
    -v $baseDir/ct_class/models:/workspace/Canon/ct_class/models\
    -v $baseDir/ct_seg/models:/workspace/Canon/ct_seg/models\
    -v $baseDir/input:/workspace/Canon/input\
    -v $baseDir/TRT_Inference:/workspace/Canon/TRT_Inference\
    $dockerImage /bin/bash

