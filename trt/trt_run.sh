#!/bin/bash

echo 'run trt for segmentation models'
trtexec --loadEngine=/workspace/Canon/ct_seg/models/model.engine

echo 'run trt for classification models'
trtexec --loadEngine=/workspace/Canon/ct_class/models/model.plan

echo 'Finished!'