import tensorflow as tf 
import nibabel as nib
import time 
from tensorflow.python.compiler.tensorrt import trt_convert as trt

frozen_graph_filename = "../ct_class/models/model.fzn.pb"
outputtrt = "./"
INPUT_SHAPE = (1,192,192,64,1)
test_image = nib.load('image_processed.nii')
test_input = test_image.get_fdata()
test_input =test_input.reshape(INPUT_SHAPE)

with tf.Session() as sess:
    # First deserialize your frozen graph:
    with tf.gfile.GFile(frozen_graph_filename, 'rb') as f:
        frozen_graph = tf.GraphDef()
        frozen_graph.ParseFromString(f.read())
    # Now you can create a TensorRT inference graph from your
    # frozen graph:
    converter = trt.TrtGraphConverter(
	    input_graph_def=frozen_graph,
	    nodes_blacklist=['NV_MODEL_OUTPUT']) #output nodes
    trt_graph = converter.convert()
    trt_graph.save(outputtrt)
    # Import the TensorRT graph into a new graph and run:
    output_node = tf.import_graph_def(
        trt_graph,
        return_elements=['NV_MODEL_OUTPUT'])
    x = graph.get_tensor_by_name('NV_MODEL_INPUT:0')
    print(sess.run(output_node, feed_dict={x: test_input}))

'''
start_time = time.time()
frozen_graph_filename = "../ct_class/models/model.graphdef"
INPUT_SHAPE = (1,192,192,64,1)
test_image = nib.load('image_processed.nii')
test_input = test_image.get_fdata()
test_input =test_input.reshape(INPUT_SHAPE)

with tf.gfile.GFile(frozen_graph_filename, "rb") as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())

#G = tf.Graph()


#with tf.Session(graph=G) as sess:
# Then, we can use again a convenient built-in function to import a graph_def into the
# current default Graph
with tf.Graph().as_default() as graph:
    tf.import_graph_def(
        graph_def,
        input_map=None,
        return_elements=None,
        name="",
        op_dict=None,
        producer_op_list=None
    )
    sess = tf.Session(graph = graph)
    load_time = time.time()
    x = graph.get_tensor_by_name('NV_MODEL_INPUT:0')
    output_tensor=graph.get_tensor_by_name('NV_MODEL_OUTPUT:0')
    print(sess.run(output_tensor, feed_dict={x: test_input}))

print("End-to-End running time",(time.time() - start_time)) 
print("loading Engine time", (load_time - start_time)) 
print("AI running time",(time.time() - load_time)) 
'''