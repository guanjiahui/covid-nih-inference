#!/bin/bash
echo 'start inference on Segmentation...'
bash infer_seg.sh 
echo 'start inference on Classification...'
bash infer_class.sh 
echo 'Finished!'