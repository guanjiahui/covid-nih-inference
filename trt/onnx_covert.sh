#!/bin/bash

echo 'convert tf2onnx for segmentation models'
python3 -m tf2onnx.convert --opset=12 --input ./ct_seg/models/model.fzn.pb --inputs NV_MODEL_INPUT:0 --outputs NV_MODEL_OUTPUT:0 --output ./ct_seg/models/model.onnx

echo 'convert tf2onnx for segmentation models'
python3 -m tf2onnx.convert --opset=12 --input ./ct_class/models/model.fzn.pb --inputs NV_MODEL_INPUT:0 --outputs NV_MODEL_OUTPUT:0 --output ./ct_class/models/model.onnx

