start inference on Segmentation...
f2f053197a33f1b9bc68c466314e48be204f6d073f3b4788aec33febc1c97513
b1db4f2d4eb5a8b5753ddc2c76849c559dd13f94d7fac0ccf7aaec43e11f4a1e
nvidia-docker run --name trtis --network container-demo -d --rm --shm-size=1g --ulimit memlock=-1 --ulimit stack=67108864 -p 8000:8000 -v /home/ubuntu/Canon/NIH_covid/tf_triton/models/segmentation_ct_lung_v1:/models/segmentation_ct_lung_v1 nvcr.io/nvidia/tensorrtserver:19.08-py3 trtserver --model-store=/models
Wait until TRTIS 172.24.0.2 is ready..........done
WARNING: Logging before flag parsing goes to stderr.
W1113 01:58:24.561525 139913833953088 module_wrapper.py:139] From /usr/local/lib/python3.6/dist-packages/horovod/tensorflow/__init__.py:101: The name tf.train.SessionRunHook is deprecated. Please use tf.estimator.SessionRunHook instead.

W1113 01:58:24.561798 139913833953088 module_wrapper.py:139] From /usr/local/lib/python3.6/dist-packages/horovod/tensorflow/__init__.py:135: The name tf.train.Optimizer is deprecated. Please use tf.compat.v1.train.Optimizer instead.

--------------------------------------------------------------------------
[[5451,1],0]: A high-performance Open MPI point-to-point messaging module
was unable to find any relevant network interfaces:

Module: OpenFabrics (openib)
  Host: dd13283471f3

Another transport will be used instead, although this may result in
lower performance.

NOTE: You can disable this warning by setting the MCA parameter
btl_base_warn_component_unused to 0.
--------------------------------------------------------------------------
{"event": {"category": "operator", "name":"processing_started", "level": "info", "timestamp": "20201113T015824.866Z"}, "message": "AI-base_inference"}
[PERF] AI-base_inference Start Time: 1605232704867
{"event": {"category": "operator", "name":"processing_started", "level": "info", "timestamp": "20201113T015824.900Z", "stage": "application setup"}, "message": "AI-base_inference Application setup"}
[PERF] AI-base_inference Application setup Start Time: 1605232704900
{"event": {"category": "operator","name":"processing_ended", "level": "info", "timestamp": "20201113T015825.281Z", "elapsed_time": 381, "stage": "application setup"},"message":"AI-base_inference Application setup"}
[PERF] AI-base_inference Application setup End Time: 1605232705281
[PERF] AI-base_inference Application setup Elapsed Time (ms): 381
{"event": {"category": "operator", "name":"processing_started", "level": "info", "timestamp": "20201113T015825.281Z", "stage": "application execute"}, "message": "AI-base_inference Application execute"}
[PERF] AI-base_inference Application execute Start Time: 1605232705281
{"event": {"category": "operator", "name":"processing_started", "level": "info", "timestamp": "20201113T015825.281Z", "stage": "input selection and conversion"}, "message": "AI-base_inference Input selection and conversion"}
[PERF] AI-base_inference Input selection and conversion Start Time: 1605232705281
W1113 01:58:25.281575 139910310364928 app.py:382] Writers in config ignored; pre-defined writer is used.
{"event": {"category": "operator","name":"processing_ended", "level": "info", "timestamp": "20201113T015825.281Z", "elapsed_time": 1, "stage": "input selection and conversion"},"message":"AI-base_inference Input selection and conversion"}
[PERF] AI-base_inference Input selection and conversion End Time: 1605232705282
[PERF] AI-base_inference Input selection and conversion Elapsed Time (ms): 1
{"event": {"category": "operator", "name":"processing_started", "level": "info", "timestamp": "20201113T015825.282Z", "stage": "transforms and inference"}, "message": "AI-base_inference Transforms and Inference"}
[PERF] AI-base_inference Transforms and Inference Start Time: 1605232705282
{"event": {"category": "operator", "name":"processing_started", "level": "info", "timestamp": "20201113T015825.282Z", "stage": "ai-base_inference pre-process"}, "message": "AI-base_inference Pre-process"}
[PERF] AI-base_inference Pre-process Start Time: 1605232705282
{"event": {"category": "operator","name":"processing_ended", "level": "info", "timestamp": "20201113T015830.066Z", "elapsed_time": 4785, "stage": "ai-base_inference pre-process"},"message":"AI-base_inference Pre-process"}
[PERF] AI-base_inference Pre-process End Time: 1605232710067
[PERF] AI-base_inference Pre-process Elapsed Time (ms): 4785
{"event": {"category": "operator", "name":"processing_started", "level": "info", "timestamp": "20201113T015830.067Z", "stage": "ai-base_inference inference"}, "message": "AI-base_inference Inference"}
[PERF] AI-base_inference Inference Start Time: 1605232710067
data: {'image': array([[[[0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.],
         ...,
         [0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.]],

        [[0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.],
         ...,
         [0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.]],

        [[0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.],
         ...,
         [0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.]],

        ...,

        [[0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.],
         ...,
         [0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.]],

        [[0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.],
         ...,
         [0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.]],

        [[0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.],
         ...,
         [0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.],
         [0., 0., 0., ..., 0., 0., 0.]]]], dtype=float32), 'image_resolution': array([0.878906, 0.878906, 1.25    ], dtype=float32), 'affine': array([[  -0.87890601,    0.        ,    0.        ,  229.6000061 ],
       [   0.        ,   -0.87890601,    0.        ,  245.19999695],
       [   0.        ,    0.        ,    1.25      , -345.        ],
       [   0.        ,    0.        ,    0.        ,    1.        ]]), 'filename': '/input/deployTest.nii', 'fileformat': 'nii', 'shape': (512, 512, 273), 'spacing': (0.878906, 0.878906, 1.25), 'image_shape': (512, 512, 273)}
{"event": {"category": "operator","name":"processing_ended", "level": "info", "timestamp": "20201113T015848.138Z", "elapsed_time": 18071, "stage": "ai-base_inference inference"},"message":"AI-base_inference Inference"}
[PERF] AI-base_inference Inference End Time: 1605232728139
[PERF] AI-base_inference Inference Elapsed Time (ms): 18071
{"event": {"category": "operator", "name":"processing_started", "level": "info", "timestamp": "20201113T015848.138Z", "stage": "ai-base_inference post-process and write"}, "message": "AI-base_inference Post-process and Write"}
[PERF] AI-base_inference Post-process and Write Start Time: 1605232728139
E1113 01:58:48.719325 139910310364928 transforms.py:2515] [cupy] out of memory to allocate 858783744 bytes (total 901740032 bytes)
Traceback (most recent call last):
  File "components/transforms/libs/transforms.py", line 2509, in __call__
  File "components/transforms/libs/transforms.py", line 2223, in resample
  File "components/transforms/libs/transforms.py", line 2178, in affine_transform
  File "/usr/local/lib/python3.6/dist-packages/cupy/indexing/generate.py", line 186, in indices
    res = cupy.empty((N,) + dimensions, dtype=dtype)
  File "/usr/local/lib/python3.6/dist-packages/cupy/creation/basic.py", line 22, in empty
    return cupy.ndarray(shape, dtype, order=order)
  File "cupy/core/core.pyx", line 134, in cupy.core.core.ndarray.__init__
  File "cupy/cuda/memory.pyx", line 518, in cupy.cuda.memory.alloc
  File "cupy/cuda/memory.pyx", line 1085, in cupy.cuda.memory.MemoryPool.malloc
  File "cupy/cuda/memory.pyx", line 1106, in cupy.cuda.memory.MemoryPool.malloc
  File "cupy/cuda/memory.pyx", line 934, in cupy.cuda.memory.SingleDeviceMemoryPool.malloc
  File "cupy/cuda/memory.pyx", line 949, in cupy.cuda.memory.SingleDeviceMemoryPool._malloc
  File "cupy/cuda/memory.pyx", line 697, in cupy.cuda.memory._try_malloc
cupy.cuda.memory.OutOfMemoryError: out of memory to allocate 858783744 bytes (total 901740032 bytes)
/usr/local/lib/python3.6/dist-packages/skimage/transform/_warps.py:105: UserWarning: The default mode, 'constant', will be changed to 'reflect' in skimage 0.15.
  warn("The default mode, 'constant', will be changed to 'reflect' in "
/usr/local/lib/python3.6/dist-packages/skimage/transform/_warps.py:110: UserWarning: Anti-aliasing will be enabled by default in skimage 0.15 to avoid aliasing artifacts when down-sampling images.
  warn("Anti-aliasing will be enabled by default in skimage 0.15 to "
{"event": {"category": "operator","name":"processing_ended", "level": "info", "timestamp": "20201113T015857.569Z", "elapsed_time": 9430, "stage": "ai-base_inference post-process and write"},"message":"AI-base_inference Post-process and Write"}
[PERF] AI-base_inference Post-process and Write End Time: 1605232737569
[PERF] AI-base_inference Post-process and Write Elapsed Time (ms): 9430
{"event": {"category": "operator","name":"processing_ended", "level": "info", "timestamp": "20201113T015857.581Z", "elapsed_time": 32299, "stage": "transforms and inference"},"message":"AI-base_inference Transforms and Inference"}
[PERF] AI-base_inference Transforms and Inference End Time: 1605232737581
[PERF] AI-base_inference Transforms and Inference Elapsed Time (ms): 32299
{"event": {"category": "operator","name":"processing_ended", "level": "info", "timestamp": "20201113T015859.761Z", "elapsed_time": 34480, "stage": "application execute"},"message":"AI-base_inference Application execute"}
[PERF] AI-base_inference Application execute End Time: 1605232739762
[PERF] AI-base_inference Application execute Elapsed Time (ms): 34480
{"event": {"category": "operator", "name":"processing_ended", "level": "info", "timestamp": "20201113T015859.765Z", "elapsed_time": 34898}, "message": "AI-base_inference"}
[PERF] AI-base_inference End Time: 1605232739765
[PERF] AI-base_inference Elapsed Time (ms): 34898
nvcr.io/ea-nvidia-clara/clara/ai-lung:0.7.2-2009.3 has finished.
Stopping Triton(TRTIS) inference server.
start inference on Classification...
d46a74cd0776b3098ef473b37e61c184601d0b00029ce38d5139658fb1f704bc
a2de17e560b91a5783a13c29b016554fade0b6f86291bf22dc6aa0d8a47261df
nvidia-docker run --name trtis --network container-demo -d --rm --shm-size=1g --ulimit memlock=-1 --ulimit stack=67108864 -p 8000:8000 -v /home/ubuntu/Canon/NIH_covid/tf_triton/models/classification_covid-19_v1:/models/classification_covid-19_v1 nvcr.io/nvidia/tensorrtserver:19.08-py3 trtserver --model-store=/models
Wait until TRTIS 172.25.0.2 is ready..........done
WARNING:tensorflow:From /usr/local/lib/python3.6/dist-packages/horovod/tensorflow/__init__.py:117: The name tf.global_variables is deprecated. Please use tf.compat.v1.global_variables instead.

WARNING:tensorflow:From /usr/local/lib/python3.6/dist-packages/horovod/tensorflow/__init__.py:143: The name tf.get_default_graph is deprecated. Please use tf.compat.v1.get_default_graph instead.

--------------------------------------------------------------------------
[[36819,1],0]: A high-performance Open MPI point-to-point messaging module
was unable to find any relevant network interfaces:

Module: OpenFabrics (openib)
  Host: fad44b808050

Another transport will be used instead, although this may result in
lower performance.

NOTE: You can disable this warning by setting the MCA parameter
btl_base_warn_component_unused to 0.
--------------------------------------------------------------------------
INFO:__main__:Program started.
{"event": {"category": "operator", "name":"processing_started", "level": "info", "timestamp": "20201113T015921.259Z"}, "message": "app_covid-19"}
[PERF] app_covid-19 Start Time: 1605232761260
{"event": {"category": "operator", "name":"processing_started", "level": "info", "timestamp": "20201113T015921.293Z", "stage": "application setup"}, "message": "Application setup"}
[PERF] Application setup Start Time: 1605232761293
INFO:app.App:Runtime env settings: {'input_folder': '/input', 'output_folder': '/output', 'label_image_folder': '/label_image', 'logs_folder': '/logs', 'publish_folder': '/publish', 'trtis_uri': '172.25.0.2:8000', 'config_inference_path': 'app_covid-19/config/config_inference.json', 'nii_extension': '.nii'}
INFO:app.App:Trying to check TRTIS server health ...
WARNING:app.App:model_loader config item is not used.
{"event": {"category": "operator","name":"processing_ended", "level": "info", "timestamp": "20201113T015921.301Z", "elapsed_time": 8, "stage": "application setup"},"message":"Application setup"}
[PERF] Application setup End Time: 1605232761301
[PERF] Application setup Elapsed Time (ms): 8
{"event": {"category": "operator", "name":"processing_started", "level": "info", "timestamp": "20201113T015921.301Z", "stage": "application execute"}, "message": "Application execute"}
[PERF] Application execute Start Time: 1605232761301
{"event": {"category": "operator", "name":"processing_started", "level": "info", "timestamp": "20201113T015921.301Z", "stage": "input selection and conversion"}, "message": "Input selection and conversion"}
[PERF] Input selection and conversion Start Time: 1605232761301
INFO:app.App:Selected input file: /input/deployTest.nii, and its extension: .nii
INFO:app.App:Selected label image file: /label_image/deployTest.mhd, and its extension: .mhd
INFO:root:MHD metadata[ITK_InputFilterName]: MetaImageIO
INFO:app.App:Label image file converted for transforms: temp_base_app/deployTest_label.nii. Extracted properties: {'Spacing': (1.0, 1.0, 1.0), 'Origin': (0.0, 0.0, 0.0), 'Direction': (1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0)}
INFO:app.App:File list for transforms: [{'image': '/input/deployTest.nii', 'label_image': 'temp_base_app/deployTest_label.nii'}]
{"event": {"category": "operator","name":"processing_ended", "level": "info", "timestamp": "20201113T015921.410Z", "elapsed_time": 109, "stage": "input selection and conversion"},"message":"Input selection and conversion"}
[PERF] Input selection and conversion End Time: 1605232761410
[PERF] Input selection and conversion Elapsed Time (ms): 109
{"event": {"category": "operator", "name":"processing_started", "level": "info", "timestamp": "20201113T015921.410Z", "stage": "transforms and inference"}, "message": "Transforms and Inference"}
[PERF] Transforms and Inference Start Time: 1605232761411
Previously evaluated: 0 ; To be evaluated: 1
INFO:root:Model output: [['0' '3.3328893184661865' 'non-COVID']
 ['1' '-3.2230587005615234' 'COVID']]
Batch 1 / 1: 10.33s; pre-process: 5.53s; infer: 4.79s; post-process: 0.00s


INFO:root:Finalizing results for input: /input/deployTest.nii
INFO:root:Class/row:['0' '0.9985804' 'non-COVID']
INFO:root:Class/row:['1' '0.0014196164' 'COVID']
Total Inference Time: 4.792971849441528s
{"event": {"category": "operator","name":"processing_ended", "level": "info", "timestamp": "20201113T015931.742Z", "elapsed_time": 10331, "stage": "transforms and inference"},"message":"Transforms and Inference"}
[PERF] Transforms and Inference End Time: 1605232771742
[PERF] Transforms and Inference Elapsed Time (ms): 10331
{"event": {"category": "operator","name":"processing_ended", "level": "info", "timestamp": "20201113T015931.742Z", "elapsed_time": 10441, "stage": "application execute"},"message":"Application execute"}
[PERF] Application execute End Time: 1605232771742
[PERF] Application execute Elapsed Time (ms): 10441
{"event": {"category": "operator", "name":"processing_ended", "level": "info", "timestamp": "20201113T015931.743Z", "elapsed_time": 10483}, "message": "app_covid-19"}
[PERF] app_covid-19 End Time: 1605232771743
[PERF] app_covid-19 Elapsed Time (ms): 10483
INFO:__main__:Program exited.
nvcr.io/ea-nvidia-clara/clara/ai-covid-19:0.7.2-2009.3 has finished.
Stopping Triton(TRTIS) inference server.
Finished!
