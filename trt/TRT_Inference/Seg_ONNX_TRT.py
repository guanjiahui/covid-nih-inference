import random
from PIL import Image
import numpy as np
import nibabel as nib
import pycuda.driver as cuda
# This import causes pycuda to automatically manage CUDA context creation and cleanup.
import pycuda.autoinit

import tensorrt as trt

import sys, os
sys.path.insert(1, os.path.join(sys.path[0], ".."))
import common

import time


        
class ModelData(object):
    MODEL_PATH = "/workspace/Canon/ct_seg/models/model22.onnx"
    #INPUT_SHAPE = (192,192,64)
    # We can convert TensorRT data types to numpy types with trt.nptype()
    DTYPE = trt.float32

# You can set the logger severity higher to suppress messages (or lower to display more messages).
TRT_LOGGER = trt.Logger(trt.Logger.WARNING)

# The Onnx path is used for Onnx models.
def build_engine_onnx(model_file):
    with trt.Builder(TRT_LOGGER) as builder, builder.create_network(common.EXPLICIT_BATCH) as network, trt.OnnxParser(network, TRT_LOGGER) as parser:
        builder.max_workspace_size = common.GiB(2)
        # Load the Onnx model and parse it in order to populate the TensorRT network.
        with open(model_file, 'rb') as model:
            if not parser.parse(model.read()):
                print ('ERROR: Failed to parse the ONNX file.')
                for error in range(parser.num_errors):
                    print (parser.get_error(error))
                return None
        return builder.build_cuda_engine(network)

def load_test_case(test_image, pagelocked_buffer):
    image_arr = test_image.get_fdata()
    image_arr = image_arr.astype(trt.nptype(ModelData.DTYPE)).ravel()
    np.copyto(pagelocked_buffer, image_arr)
    return test_image

def main():
    # Get test images, models and labels.
    test_image = nib.load('image_processed_seg.nii')
    outputfile="mask_trt.npy"
    onnx_model_file= "/workspace/Canon/ct_seg/models/model.onnx"
    saveEngine="/workspace/Canon/ct_seg/models/model.plan"


    # Build a TensorRT engine.
    with build_engine_onnx(onnx_model_file) as engine, open(saveEngine, "wb") as f:
        # Inference is the same regardless of which parser is used to build the engine, since the model architecture is the same.
        # Allocate buffers and create a CUDA stream.
        inputs, outputs, bindings, stream = common.allocate_buffers(engine)
        # Contexts are used to perform inference.
        with engine.create_execution_context() as context:
 
            test_case = load_test_case(test_image, inputs[0].host)
    
            trt_outputs = common.do_inference_v2(context, bindings=bindings, inputs=inputs, outputs=outputs, stream=stream)
            # We use the highest probability as our prediction. Its index corresponds to the predicted label.
            np.save(outfile, trt_outputs)
        f.write(engine.serialize())

if __name__ == '__main__':
    main()
