#!/bin/bash

baseDir="/home/ubuntu/Canon/NIH_covid/trt"
dockerImage=trtconvert:latest

docker run --gpus all -it --rm \
    -v $baseDir/ct_class/models:/workspace/Canon/ct_class/models\
    -v $baseDir/ct_seg/models:/workspace/Canon/ct_seg/models\
    $dockerImage /bin/bash
