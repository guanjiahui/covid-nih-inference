import random
from PIL import Image
import numpy as np
import nibabel as nib
import pycuda.driver as cuda
# This import causes pycuda to automatically manage CUDA context creation and cleanup.
import pycuda.autoinit

import tensorrt as trt

import sys, os
sys.path.insert(1, os.path.join(sys.path[0], ".."))
import common

import time


        
class ModelData(object):
    MODEL_PATH = "/workspace/Canon/ct_class/models/model.onnx"
    INPUT_SHAPE = (192,192,64)
    # We can convert TensorRT data types to numpy types with trt.nptype()
    DTYPE = trt.float32

# You can set the logger severity higher to suppress messages (or lower to display more messages).
TRT_LOGGER = trt.Logger(trt.Logger.WARNING)

# The Onnx path is used for Onnx models.
def build_engine_onnx(model_file):
    with trt.Builder(TRT_LOGGER) as builder, builder.create_network(common.EXPLICIT_BATCH) as network, trt.OnnxParser(network, TRT_LOGGER) as parser:
        builder.max_workspace_size = common.GiB(1)
        # Load the Onnx model and parse it in order to populate the TensorRT network.
        with open(model_file, 'rb') as model:
            if not parser.parse(model.read()):
                print ('ERROR: Failed to parse the ONNX file.')
                for error in range(parser.num_errors):
                    print (parser.get_error(error))
                return None
        return builder.build_cuda_engine(network)

def load_test_case(test_image, pagelocked_buffer):
    image_arr = test_image.get_fdata()
    image_arr = image_arr.astype(trt.nptype(ModelData.DTYPE)).ravel()
    np.copyto(pagelocked_buffer, image_arr)
    return test_image

def main():
    # Get test images, models and labels.
    test_image = nib.load('image_processed.nii')
    #mask = nib.load('mask_processed.nii')

    start_time = time.time()

    print ("loading TensorRT Engine...")
    with open("/workspace/Canon/ct_class/models/model.engine", "rb") as f, trt.Runtime(TRT_LOGGER) as runtime:
        engine = runtime.deserialize_cuda_engine(f.read())
 
        inputs, outputs, bindings, stream = common.allocate_buffers(engine)
        print("Engine loaded")
        load_time = time.time()
        with engine.create_execution_context() as context:
            test_case = load_test_case(test_image, inputs[0].host)
            print("test data loaded")
            
            trt_outputs = common.do_inference_v2(context, bindings=bindings, inputs=inputs, outputs=outputs, stream=stream)

            print(trt_outputs)
    print("End-to-End running time",(time.time() - start_time)) 
    print("loading Engine time", (load_time - start_time)) 
    print("AI running time",(time.time() - load_time)) 
    
if __name__ == '__main__':
    main()
