#!/bin/bash

echo 'convert onnx2trt for segmentation models'
trtexec --onnx=/workspace/Canon/ct_seg/models/model.onnx --shapes=NV_MODEL_INPUT:0:1x1x320x320x64 --explicitBatch --workspace=3000 --saveEngine=/workspace/Canon/ct_seg/models/model.plan

echo 'convert onnx2trt for classification models'
trtexec --onnx=/workspace/Canon/ct_class/models/model.onnx --shapes=NV_MODEL_INPUT:0:1x192x192x64x1 --explicitBatch --workspace=1000 --saveEngine=/workspace/Canon/ct_class/models/model_v20_07.plan

echo 'Finished!'