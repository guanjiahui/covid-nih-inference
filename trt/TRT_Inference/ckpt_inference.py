import tensorflow as tf 
import nibabel as nib
import time 
from tensorflow.python.compiler.tensorrt import trt_convert as trt
#import horovod.tensorflow as hvd


start_time = time.time()
frozen_graph_filename = "../ct_class/models/model.fzn.pb"
INPUT_SHAPE = (1,192,192,64,1)
test_image = nib.load('image_processed.nii')
test_input = test_image.get_fdata()
test_input =test_input.reshape(INPUT_SHAPE)

with tf.gfile.GFile(frozen_graph_filename, "rb") as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())

#G = tf.Graph()


#with tf.Session(graph=G) as sess:
# Then, we can use again a convenient built-in function to import a graph_def into the
# current default Graph
with tf.Graph().as_default() as graph:
    tf.import_graph_def(
        graph_def,
        input_map=None,
        return_elements=None,
        name="",
        op_dict=None,
        producer_op_list=None
    )
    sess = tf.Session(graph = graph)
    load_time = time.time()
    x = graph.get_tensor_by_name('NV_MODEL_INPUT:0')
    output_tensor=graph.get_tensor_by_name('NV_MODEL_OUTPUT:0')
    print(sess.run(output_tensor, feed_dict={x: test_input}))

print("End-to-End running time",(time.time() - start_time)) 
print("loading Engine time", (load_time - start_time)) 
print("AI running time",(time.time() - load_time)) 

'''
start_time = time.time()
model_filename = "../ct_class/models/model.ckpt.meta"
INPUT_SHAPE = (1,192,192,64,1)
test_image = nib.load('image_processed.nii')
test_input = test_image.get_fdata()
test_input =test_input.reshape(INPUT_SHAPE)

sess=tf.Session()    
#First let's load meta graph and restore weights
saver = tf.train.import_meta_graph(model_filename)
saver.restore(sess,tf.train.latest_checkpoint('./'))

graph = tf.get_default_graph()

load_time = time.time()
x = graph.get_tensor_by_name('NV_MODEL_INPUT:0')
output_tensor=graph.get_tensor_by_name('NV_MODEL_OUTPUT:0')
print(sess.run(output_tensor, feed_dict={x: test_input}))

print("End-to-End running time",(time.time() - start_time)) 
print("loading Engine time", (load_time - start_time)) 
print("AI running time",(time.time() - load_time)) 
'''